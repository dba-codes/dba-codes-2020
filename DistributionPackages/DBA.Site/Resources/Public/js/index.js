function menu() {
	$('.menu-toggle').click(function() {
		var clicks = $(this).data('clicks');
		if (clicks) {

			$('.menu-toggle').removeClass('active');
			$('nav.nav').removeClass('show');
			$('header.header').removeClass('active-nav');
			$('body').removeClass('deactive');

			function remove() {
				$('nav.nav').removeClass('add');
			}

			setTimeout(remove, 350);

		} else {

			$('nav.nav').addClass('add');

			function add() {
                $('.menu-toggle').addClass('active');
				$('nav.nav').addClass('show');
				$('header.header').addClass('active-nav');
                $('body').addClass('deactive');
			}

			setTimeout(add, 10);

		}
		$(this).data("clicks", !clicks);
	});
}

$(function() {

    var $nav = $('nav.default-menu');
    var $btn = $('nav.default-menu .default-menu-toggle');
    var $vlinks = $('nav.default-menu .links');
    var $hlinks = $('nav.default-menu .hidden-links');

    var numOfItems = 0;
    var totalSpace = 0;
    var breakWidths = [];

    // Get initial state
    $vlinks.children().outerWidth(function(i, w) {
        totalSpace += w;
        numOfItems += 1;
        breakWidths.push(totalSpace);
    });

    var availableSpace, numOfVisibleItems, requiredSpace;

    function check() {

        // Get instant state
        availableSpace = $vlinks.width() - $btn.width() + 30;
        numOfVisibleItems = $vlinks.children().length;
        requiredSpace = breakWidths[numOfVisibleItems - 1];

        // There is not enought space
        if (requiredSpace > availableSpace) {
            $vlinks.children().last().prependTo($hlinks);
            numOfVisibleItems -= 1;
            check();
            // There is more than enough space
        } else if (availableSpace > breakWidths[numOfVisibleItems]) {
            $hlinks.children().first().appendTo($vlinks);
            numOfVisibleItems += 1;
        }
        // Update the button accordingly
        $btn.attr("count", numOfItems - numOfVisibleItems);
        if (numOfVisibleItems === numOfItems) {
            $btn.addClass('hidden');
        } else $btn.removeClass('hidden');
    }

    // Window listeners
    $(window).resize(function() {
        check();
    });

    $btn.on('click', function() {
        $hlinks.toggleClass('hidden');
    });

    check();

});

//AOS
function aos() {
	AOS.init({
		throttleDelay: 100,
		offset: 100,
		delay: 100,
		duration: 750,
		easing: 'ease-in-out-circ',
		mirror: false
	});
}


//TAWK CHAT
function tawkChatGerman() {
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5d8b726f6c1dde20ed036852/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
}
function tawkChatEnglish() {
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5d8b726f6c1dde20ed036852/1dll0l03q';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
}

//GOOGLE ANALYTICS
function loadGAonConsent(){
	window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
	ga('create', 'UA-148741066-1', 'auto');
	ga('set', 'anonymizeIp', true);
	ga('send', 'pageview');
	var gascript = document.createElement("script");
	gascript.async = true;
	gascript.src = "https://www.google-analytics.com/analytics.js";
	document.getElementsByTagName("head")[0].appendChild(gascript, document.getElementsByTagName("head")[0]);
}


//COOKIES
function deleteAllCookies() {
	var cookies = document.cookie.split(";");

	for (var i = 0; i < cookies.length; i++) {
		var cookie = cookies[i];
		var eqPos = cookie.indexOf("=");
		var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
		document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
	}
}

if (document.cookie.split(';').filter(function(item) {
	return item.indexOf('cookieconsent_status=allow') >= 0}).length) {
	loadGAonConsent();
    getLanguage();
}

document.addEventListener("kd-cookieconsent", function (e) {
	if (e.detail === 'enable-cookies') {
		loadGAonConsent();
        getLanguage();
	} else if (e.detail === 'disable-cookies') {
		deleteAllCookies();
	}
});

//LOAD CHAT IN CORRECT LANGUAGE
function getLanguage() {
    if (window.location.href.indexOf('')!=-1) {
        /*
        	addScript('//userlike-cdn-widgets.s3-eu-west-1.amazonaws.com/fae1c7c0592f2cdbe272dd3f4b72f25ef387774930195d573db8f893c821da28.js');
    	*/
        tawkChatGerman();
    }
    if (window.location.href.indexOf('/en/')!=-1) {
		/*
			addScript('//userlike-cdn-widgets.s3-eu-west-1.amazonaws.com/b02b0752d49a96ee035b0aa13064088e2087bd170e17ecff0415635aaf3d297a.js');
    	*/
        tawkChatEnglish();
	}
}

/*
function addScript(src) {
	var s = document.createElement( 'script' );
	s.setAttribute( 'src', src );
	document.body.appendChild( s );
}
*/


$(document).ready(function() {
	menu();
	aos();
});
