<?php
namespace DBA\Site\Eel\FlowQuery;
use Neos\Eel\FlowQuery\FlowQuery;
use Neos\Eel\FlowQuery\Operations\AbstractOperation;
class FilterByTopicsOperation extends AbstractOperation {

	/**
	 * {@inheritdoc}
	 *
	 * @var string
	 */
	static protected $shortName = 'filterByTopics';

	/**
	 * {@inheritdoc}
	 *
	 * @param FlowQuery $flowQuery the FlowQuery object
	 * @param array $arguments the arguments for this operation
	 * @return void
	 */
	public function evaluate(FlowQuery $flowQuery, array $arguments) {
		$filteredContext = [];
		$context = $flowQuery->getContext();
		foreach ($context as $element) {
			if (count(array_intersect($arguments[0], $element->getProperty('article-topics'))) > 0) {
				$filteredContext[] = $element;
			}
		}
		$flowQuery->setContext($filteredContext);
	}
}

?>
