<?php
namespace DBA\Site\Eel\FlowQuery;
use Neos\Eel\FlowQuery\FlowQuery;
use Neos\Eel\FlowQuery\Operations\AbstractOperation;
class FilterByIsNewOperation extends AbstractOperation {

	/**
	 * {@inheritdoc}
	 *
	 * @var string
	 */
	static protected $shortName = 'filterByIsNew';

	/**
	 * {@inheritdoc}
	 *
	 * @param FlowQuery $flowQuery the FlowQuery object
	 * @param array $arguments the arguments for this operation
	 * @return void
	 */
	public function evaluate(FlowQuery $flowQuery, array $arguments) {
		$filteredContext = [];
		$context = $flowQuery->getContext();

		foreach ($context as $node) {
			$publishingDate = $node->getProperty('publishing-date');
			$isNew = $node->getProperty('is-new');
			$today = $arguments[0];

			$datediff = $publishingDate->diff($today);
			$dateformat = $datediff->format('%a');

			if ($dateformat <= $isNew) {
				$filteredContext[] = $node;
			}
		}
		$flowQuery->setContext($filteredContext);
	}
}

?>

