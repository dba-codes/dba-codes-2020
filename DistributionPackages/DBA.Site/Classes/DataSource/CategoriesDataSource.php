<?php
namespace DBA\Site\DataSource;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Persistence\PersistenceManagerInterface;
use Neos\Neos\Service\DataSource\AbstractDataSource;
use Neos\ContentRepository\Domain\Service\ContextFactory;
use Neos\ContentRepository\Domain\Model\NodeInterface;

class CategoriesDataSource extends AbstractDataSource
{

	/**
	 * @var string
	 */
	static protected $identifier = 'dba-site-categories';

	/**
	 * @Flow\Inject
	 * @var ContextFactory
	 */
	protected $contextFactory;

	/**
	 * @param NodeInterface $node The node that is currently edited (optional)
	 * @param array $arguments Additional arguments (key / value)
	 * @return array
	 */
	public function getData(NodeInterface $node = null, array $arguments = []) {
		$context = $this->contextFactory->create(array('workspaceName' => 'live', 'currentDateTime' => new \Neos\Flow\Utility\Now(), 'dimensions' => array(), 'invisibleContentShown' => FALSE, 'removedContentShown' => FALSE, 'inaccessibleContentShown' => FALSE));
		$site = $context->getRootNode();

		$nodes = (new \Neos\Eel\FlowQuery\FlowQuery([$site]))
			->find('[instanceof DBA.Site:Document.Category]')
			->context(array('workspaceName' => 'live'))
			->get();

		$options = [];
		foreach ($nodes as $node) {
			$options[$node->getProperty('title')] = ['label' => $node->getProperty('title')];
		}
		return $options;
	}
}